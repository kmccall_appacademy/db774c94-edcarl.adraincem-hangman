require 'byebug'

class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(options = {})
    @guesser = options[:guesser]
    @referee = options[:referee]
    @board = options[:board]
  end

  def setup
    length = referee.pick_secret_word
    guesser.register_secret_length(length)
    @board = Array.new(length) { nil }
  end

  def take_turn
    input = guesser.guess
    index = referee.check_guess(input)
    update_board
    guesser.handle_response(input, index)
  end

  def update_board

  end

end

# ---------------------------------------------------------------------
# ---------------------------------------------------------------------

class HumanPlayer
end

# ---------------------------------------------------------------------
# ---------------------------------------------------------------------

class ComputerPlayer
  attr_reader :candidate_words

  def self.dictionary_file(filename)
    ComputerPlayer.new(File.readlines(filename).map(&:chomp))
  end

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary[rand(@dictionary.length)]
    @secret_word.length
  end

  def check_guess(guess)
    ch_idx = []
    word = @secret_word.chars
    word.each_with_index { |ch, idx| ch_idx << idx if ch == guess }
    ch_idx
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select { |word| word.length == length }
  end

  def guess(board)
    letter = nil
    letter_count.each do |el|
      if !board.include?(el[0])
        letter = el[0]
        break
      end
    end
    letter
  end

  def letter_count
    letter_hash = Hash.new(0)
    joined_words = candidate_words.join
    joined_words.each_char { |ch| letter_hash[ch] += 1 }
    letter_hash.sort_by { |_k, v| v }.reverse
  end

  def handle_response(letter, idx_arr)
    @candidate_words.each_with_index do |word, idx1|

      word.each_char.with_index do |ch, idx2|
        if letter == ch && !idx_arr.include?(idx2)
          @candidate_words.slice!(idx1)
          break
        elsif letter != ch && idx_arr.include?(idx2)
          @candidate_words.slice!(idx1)
          break
        end
      end

    end
  end

end
